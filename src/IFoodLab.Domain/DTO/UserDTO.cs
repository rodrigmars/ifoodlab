﻿using IFoodLab.Domain.Notifications;
using System;
using System.Collections.Generic;

namespace IFoodLab.Domain.DTO
{
    public class UserDTO: NotificationBase
    {
        public string FirstName { get; set; }

        public long CPF { get; set; }

        public string Email { get; set; }

        public DateTime BirthDate { get; set; }

        public List<UserDTO> ListUsers { get; set; }

        public override void Add(string error) => Errors.Add(error);
    }
}
