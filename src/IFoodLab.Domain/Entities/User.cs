﻿using System;

namespace IFoodLab.Domain.Entities
{
    public class User
    {
        public User() { }

        public User(
            string firstName,
            string fullName,
            long cpf,
            DateTime birthDate,
            string phoneNumber,
            string email,
            string password,
            string address,
            string number,
            string neighborhood,
            string city,
            string cep,
            string uf,
            bool status)
        {
            FirstName = firstName;
            FullName = fullName;
            CPF = cpf;
            BirthDate = birthDate;
            PhoneNumber = phoneNumber;
            Email = email;
            Password = password;
            Address = address;
            Number = number;
            Neighborhood = neighborhood;
            City = city;
            CEP = cep;
            UF = uf;
            Status = status;
        }

        public int UserId { get; private set; }

        public string FirstName { get; private set; }

        public string FullName { get; private set; }

        public long CPF { get; private set; }

        public DateTime BirthDate { get; private set; }

        public string PhoneNumber { get; private set; }

        public string Email { get; private set; }

        public string Password { get; private set; }

        public string Address { get; private set; }

        public string Number { get; private set; }

        public string Neighborhood { get; private set; }

        public string City { get; private set; }

        public string CEP { get; private set; }

        public string UF { get; private set; }

        public bool Status { get; private set; }

        public bool EmailConfirmed { get; private set; }

        public int AccessFailedCount { get; private set; }

        public DateTime CreationDate { get; private set; }
    }
}