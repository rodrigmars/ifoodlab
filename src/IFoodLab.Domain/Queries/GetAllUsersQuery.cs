﻿using IFoodLab.Domain.DTO;
using MediatR;

namespace IFoodLab.Domain.Queries
{
    public class GetAllUsersQuery : IRequest<UserDTO> { }
}
