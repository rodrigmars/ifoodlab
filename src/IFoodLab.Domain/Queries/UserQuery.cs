﻿using IFoodLab.Domain.DTO;
using MediatR;

namespace IFoodLab.Domain.Queries
{
    public class UserQuery : IRequest<UserDTO>
    {
        public int UserId { get; set; }
    }
}
