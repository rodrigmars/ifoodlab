﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using FluentValidation;
using FluentValidation.Results;
using IFoodLab.Domain.Notifications;
using MediatR;

namespace IFoodLab.Domain.Behavior
{
    public class FailFastRequestBehavior<TRequest, TResponse> : IPipelineBehavior<TRequest, TResponse> 
        where TRequest : IRequest<TResponse> where TResponse : NotificationBase
    {
        private readonly IEnumerable<IValidator> _validators;
        private readonly NotificationBase _notification;

        public FailFastRequestBehavior(IEnumerable<IValidator<TRequest>> validators, NotificationBase notification)
        {
            _validators = validators;
            _notification = notification;
        }

        public Task<TResponse> Handle(TRequest request, CancellationToken cancellationToken, RequestHandlerDelegate<TResponse> next)
        {
            var failures = _validators
               .Select(v => v.Validate(request))
               .SelectMany(result => result.Errors)
               .Where(f => f != null)
               .ToList();

            return failures.Any()
                ? Errors(failures)
                : next();
        }

        private Task<TResponse> Errors(IEnumerable<ValidationFailure> failures)
        {           
            foreach (var failure in failures)
            {
                _notification.Add(failure.ErrorMessage);
            }

            return Task.FromResult(_notification as TResponse) ;
        }
    }
}
