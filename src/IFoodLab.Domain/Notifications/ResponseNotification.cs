﻿namespace IFoodLab.Domain.Notifications
{
    public class ResponseNotification : NotificationBase
    {
        public override void Add(string error) => Errors.Add(error);
    }
}