﻿using System.Collections.Generic;
using System.Linq;

namespace IFoodLab.Domain.Notifications
{
    public abstract class NotificationBase
    {
        public NotificationBase() => Errors = new List<string>();

        public IList<string> Errors { get; }

        public abstract void Add(string error);

        public bool HasNotifications { get => Errors.Count() > 0; }

    }
}