﻿using FluentValidation;
using IFoodLab.Domain.Commands;

namespace IFoodLab.Domain.Validations
{
    public class CreateUserValidator : AbstractValidator<CreateUserCommand>
    {
        public CreateUserValidator()
        {
            RuleFor(a => a.FirstName)
                .NotEmpty()
                .WithMessage("Primeiro nome obrigatório");

            RuleFor(a => a.FullName)
                .NotEmpty()
                .WithMessage("Nome completo obrigatório");

            RuleFor(a => a.Email)
                .EmailAddress()
                .WithMessage("O E-mail apresenta um formato inválido");

            RuleFor(a => a.Password)
                .NotEmpty()
                .WithMessage("Senha é obrigatória");

            //RuleFor(a => a.ConfirmPassword)
            //    .NotEmpty()
            //    .WithMessage("Confirmação de Senha é obrigatória")
            //    .Equal(b => b.Password)
            //    .WithMessage("As Senhas não conferem");
        }
    }
}