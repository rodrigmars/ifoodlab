﻿using IFoodLab.Domain.Interfaces;
using IFoodLab.Domain.Notifications;
using MediatR;
using System;

namespace IFoodLab.Domain.Commands
{
    public abstract class UserCommand : IRequest<NotificationBase>
    {
        public int UserId { get; set; }

        public string FirstName { get; set; }

        public string FullName { get; set; }

        public long CPF { get; set; }

        public DateTime BirthDate { get; set; }

        public string PhoneNumber { get; set; }

        public string Email { get; set; }

        public string Password { get; set; }

        public string Address { get; set; }

        public string Number { get; set; }

        public string Neighborhood { get; set; }

        public string City { get; set; }

        public string CEP { get; set; }

        public string UF { get; set; }

        public bool Status { get; set; }

        public bool EmailConfirmed { get; set; }

        public int AccessFailedCount { get; set; }

        public DateTime CreationDate { get; set; }
    }
}