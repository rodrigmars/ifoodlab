﻿using MediatR;
using System;

namespace IFoodLab.Domain.Commands
{
    public abstract class OrderCommand : IRequest
    {
        public int OrderId { get; set; }

        public DateTime OrderDate { get; set; }

        public DateTime ShipDate { get; set; }

        public bool Modified { get; set; }
    }
}