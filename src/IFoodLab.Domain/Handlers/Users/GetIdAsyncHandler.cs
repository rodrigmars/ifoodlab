﻿using IFoodLab.Domain.DTO;
using IFoodLab.Domain.Entities;
using IFoodLab.Domain.Interfaces;
using IFoodLab.Domain.Queries;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace IFoodLab.Domain.Handlers.Users
{
    public class GetIdAsyncHandler : IRequestHandler<UserQuery, UserDTO>
    {
        private readonly IUnitOfWork _unitOfWork;

        public GetIdAsyncHandler(IUnitOfWork unitOfWork) => _unitOfWork = unitOfWork;

        public async Task<UserDTO> Handle(UserQuery request, CancellationToken cancellationToken)
        {
            var user = await _unitOfWork.UserRepository.GetAsync(
                new { id = request.UserId }, @"SELECT 
                                U.FIRSTNAME, 
                                U.CPF, 
                                U.EMAIL, 
                                U.BIRTHDATE
                                FROM [User] AS U ORDER BY U.ID");

            return new UserDTO
            {
                FirstName = user.FirstName,
                CPF = user.CPF,
                Email = user.Email,
                BirthDate = user.BirthDate,
            };
        }
    }
}
