﻿using System.Threading;
using System.Threading.Tasks;
using IFoodLab.Domain.Commands;
using IFoodLab.Domain.Entities;
using IFoodLab.Domain.Interfaces;
using IFoodLab.Domain.Notifications;
using MediatR;

namespace IFoodLab.Domain.Handlers.Users
{
    public class CreateUserAsyncHandler : IRequestHandler<CreateUserCommand, NotificationBase>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly NotificationBase _notification;

        public CreateUserAsyncHandler(IUnitOfWork unitOfWork, NotificationBase notification)
        {
            _unitOfWork = unitOfWork;
            _notification = notification;
        }

        public async Task<NotificationBase> Handle(CreateUserCommand request, CancellationToken cancellationToken)
        {
            request.UserId = await _unitOfWork.UserRepository.CreateAsync(
                new User(request.FirstName,
                    request.FullName,
                    request.CPF,
                    request.BirthDate,
                    request.PhoneNumber,
                    request.Email,
                    request.Password,
                    request.Address,
                    request.Number,
                    request.Neighborhood,
                    request.City,
                    request.CEP,
                    request.UF,
                    request.Status));

            return _notification;
        }
    }
}