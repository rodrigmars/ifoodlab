﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using IFoodLab.Domain.DTO;
using IFoodLab.Domain.Interfaces;
using IFoodLab.Domain.Queries;
using MediatR;

namespace IFoodLab.Domain.Handlers.Users
{
    public class GetAllAsyncHandler : IRequestHandler<GetAllUsersQuery, UserDTO>
    {
        private readonly IUnitOfWork _unitOfWork;

        public GetAllAsyncHandler(IUnitOfWork unitOfWork) => _unitOfWork = unitOfWork;

        public async Task<UserDTO> Handle(GetAllUsersQuery request, CancellationToken cancellationToken)
        {
            var users = await _unitOfWork.UserRepository.GetAllAsync(@"SELECT 
                                U.ID AS USERID,
                                U.FIRSTNAME, 
                                U.CPF, 
                                U.EMAIL, 
                                U.birthdate
                                FROM[User] AS U ORDER BY U.ID DESC");

            var dto = new UserDTO 
            {
                ListUsers = new List<UserDTO>()
            };

            foreach (var user in users)
            {
                dto.ListUsers.Add(new UserDTO
                {
                    FirstName = user.FirstName,
                    Email = user.Email,
                    CPF = user.CPF,
                    BirthDate = user.BirthDate
                });
            }

            return dto;
        }
    }
}