﻿using IFoodLab.Domain.Commands;
using IFoodLab.Domain.Interfaces;
using IFoodLab.Domain.Notifications;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace IFoodLab.Domain.Handlers.Users
{
    public class InactiveAccessAsyncHandler : IRequestHandler<InactiveUserCommand, NotificationBase>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly NotificationBase _notification;

        public InactiveAccessAsyncHandler(IUnitOfWork unitOfWork, NotificationBase notification)
        {
            _unitOfWork = unitOfWork;
            _notification = notification;
        }


        public async Task<NotificationBase> Handle(InactiveUserCommand request, CancellationToken cancellationToken)
        {
            using (var uow = _unitOfWork)
            {
                _unitOfWork.OpenAsync();

                var user = await _unitOfWork.UserRepository.GetAsync(new { id = request.UserId },
                    @"SELECT U.ID AS USERID FROM [User] AS U WITH (NOLOCK) WHERE U.ID = @ID");

                if (user != null)
                {
                    await _unitOfWork.UserRepository.UpdateAsync(user.UserId, @"UPDATE [User] SET STATUS = 0 WHERE ID = @ID");
                }
                else
                {
                    _notification.Add("Falha ao tentar inativar usuário, conta não localizado.");
                }

                return _notification;
            }
        }
    }
}
