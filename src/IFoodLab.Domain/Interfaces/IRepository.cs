﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace IFoodLab.Domain.Interfaces
{
    public interface IRepository<T> where T : class
    {
        Task<int> AddAsync(T entity, string sqlQuery);

        Task<T> GetAsync(object obj, string sqlQuery);

        Task<IEnumerable<T>> GetAllAsync(string sqlQuery);

        Task<int> UpdateAsync(int id, string sqlQuery);

        Task<int> DeleteAsync(int id, string sqlQuery);
    }
}
