﻿using System;

namespace IFoodLab.Domain.Interfaces
{
    public interface IUnitOfWork : IDisposable
    {
        IUserRepository UserRepository { get; }

        void Open();

        void OpenAsync();

        void Begin();

        void Commit();

        void ResetRepositories();
    }
}
