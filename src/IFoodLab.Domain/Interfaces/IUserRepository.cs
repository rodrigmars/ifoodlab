﻿using IFoodLab.Domain.Entities;
using System.Threading.Tasks;

namespace IFoodLab.Domain.Interfaces
{
    public interface IUserRepository : IRepository<User>
    {
        Task<int> CreateAsync(User user);
    }
}