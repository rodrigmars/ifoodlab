﻿using FluentValidation;
using IFoodLab.Data.Behavior;
using IFoodLab.Data.Repositories;
using IFoodLab.Data.UOW;
using IFoodLab.Domain.Behavior;
using IFoodLab.Domain.Interfaces;
using IFoodLab.Domain.Notifications;
using MediatR;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace IFoodLab.API.Extensions
{
    public static class ServiceExtensions
    {
        public static void ConfigureCors(this IServiceCollection services)
        {
            services.AddCors(options =>
            {
                options.AddPolicy("CorsPolicy",
                    builder => builder.AllowAnyOrigin()
                    .AllowAnyMethod()
                    .AllowAnyHeader()
                    .AllowCredentials());
            });
        }

        public static void ConfigureDependencyInjection(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddSingleton(configuration)
            .AddTransient<IUnitOfWork, UnitOfWork>()
            .AddScoped(typeof(IRepository<>), typeof(Repository<>))
            .AddScoped<IUserRepository, UserRepository>()
            .AddScoped<NotificationBase, ResponseNotification>()
            .BuildServiceProvider();
        }

        public static void ConfigureMediatRPlusFluentValidation(this IServiceCollection services)
        {
            var domainAssembly = AppDomain.CurrentDomain.Load("IFoodLab.Domain");

            AssemblyScanner.FindValidatorsInAssembly(domainAssembly)
                .ForEach(result => services.AddScoped(result.InterfaceType, result.ValidatorType));

            services.AddScoped(typeof(IPipelineBehavior<,>), typeof(FailFastRequestBehavior<,>))
            .AddScoped(typeof(IPipelineBehavior<,>), typeof(UnitOfWorkAsyncBehavior<,>))
            .AddMediatR(domainAssembly);
        }
    }
}
