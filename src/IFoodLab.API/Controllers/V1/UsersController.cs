﻿using System.Linq;
using System.Threading.Tasks;
using IFoodLab.Domain.Commands;
using IFoodLab.Domain.Queries;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace IFoodLab.API.Controllers.V1
{
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly IMediator _mediator;

        public UsersController(IMediator mediator) => _mediator = mediator;

        // GET: api/Users/
        [HttpGet()]
        public async Task<ActionResult> GetAsync()
        {
            var response = await _mediator.Send(new GetAllUsersQuery());

            if (response.Errors.Any()) return BadRequest(response.Errors);

            return Ok(response.ListUsers);
        }

        // GET: api/Users/5
        [HttpGet("{id}", Name = "GetUserAsync")]
        [ProducesResponseType(404)]
        public async Task<ActionResult> GetAsync(int id)
        {
            var response = await _mediator.Send(new UserQuery { UserId = id });

            if (response.Errors.Any()) return BadRequest(response.Errors);

            if (response == null) return NotFound();

            return Ok(response);
        }

        // POST: api/Users
        [HttpPost]
        [ProducesResponseType(201)]
        [ProducesResponseType(400)]
        public async Task<IActionResult> CreateAsync([FromBody] CreateUserCommand command)
        {
            var response = await _mediator.Send(command);

            if (response.Errors.Any()) return BadRequest(response.Errors);

            return CreatedAtAction("GetAsync", new { id = command.UserId }, command);
        }

        // PUT: api/Users/5
        [HttpPut("{id}")]
        public async Task Put(int id, [FromBody] UserCommand command) => await _mediator.Send(command);

        // DELETE: api/Users/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> Inactivate(int id)
        {
            var response = await _mediator.Send(new InactiveUserCommand { UserId = id });

            if (response.Errors.Any()) return BadRequest(response.Errors);

            return Ok();
        }
    }
}