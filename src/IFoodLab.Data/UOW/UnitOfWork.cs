﻿using IFoodLab.Data.DbContext;
using IFoodLab.Data.Repositories;
using IFoodLab.Domain.Interfaces;
using Microsoft.Extensions.Configuration;
using System;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;

namespace IFoodLab.Data.UOW
{
    public class UnitOfWork : ConfigConnection, IUnitOfWork
    {
        private IDbTransaction _transaction { get; set; }
        private DbConnection _connection { get; set; }

        private bool _disposed;

        public UnitOfWork(IConfiguration config) : base(config)
            => _connection = new SqlConnection(ConnectionString);

        private IUserRepository _userRepository;

        public IUserRepository UserRepository
        {
            get => _userRepository ??
                (_userRepository = new UserRepository(_transaction?.Connection ?? _connection, _transaction));
        }

        public void Open() => OpenConnection(TypeConnection.Sync);

        public void OpenAsync() => OpenConnection(TypeConnection.Async);

        private void OpenConnection(TypeConnection type)
        {
            if (_connection.State == ConnectionState.Closed)
            {
                if (type == TypeConnection.Sync) _connection.Open();

                if (type == TypeConnection.Async) _connection.OpenAsync();
            }
        }

        public void Begin() => _transaction = _connection.BeginTransaction();

        public void Commit()
        {
            if (_transaction != null)
            {
                try
                {
                    _transaction.Commit();
                }
                catch
                {
                    _transaction.Rollback();
                    throw;
                }
                finally
                {
                    _transaction.Dispose();
                    ResetRepositories();
                }
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    if (_transaction != null)
                    {
                        _transaction.Dispose();
                        _transaction = null;
                    }
                    if (_connection != null)
                    {
                        _connection.Dispose();
                        _connection = null;
                    }
                }
                _disposed = true;
            }
        }

        public void ResetRepositories()
        {
            _userRepository = null;
        }

        ~UnitOfWork() => Dispose(false);
    }
}