﻿namespace IFoodLab.Data.UOW
{
    internal enum TypeConnection
    {
        Sync,
        Async
    }
}
