﻿using System.Threading;
using System.Threading.Tasks;
using IFoodLab.Domain.Interfaces;
using MediatR;

namespace IFoodLab.Data.Behavior
{
    public class UnitOfWorkAsyncBehavior<TRequest, TResponse> : IPipelineBehavior<TRequest, TResponse>
    {
        private readonly IUnitOfWork _unitOfWork;

        public UnitOfWorkAsyncBehavior(IUnitOfWork unitOfWork) => _unitOfWork = unitOfWork;

        public async Task<TResponse> Handle(TRequest request, CancellationToken cancellationToken, RequestHandlerDelegate<TResponse> next)
        {
            using (var uow = _unitOfWork)
            {
                _unitOfWork.OpenAsync();

                var result = await next();

                _unitOfWork.Commit();

                return result;
            }
        }
    }
}