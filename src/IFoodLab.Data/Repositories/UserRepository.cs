﻿using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;
using IFoodLab.Domain.Entities;
using IFoodLab.Domain.Interfaces;

namespace IFoodLab.Data.Repositories
{
    public class UserRepository : Repository<User>, IUserRepository
    {
        public UserRepository(IDbConnection dbConnection, IDbTransaction dbTransaction = null)
            : base(dbConnection, dbTransaction) { }

        public async Task<int> CreateAsync(User user)
        {
            return await AddAsync(user, @"INSERT INTO [DBO].[USER]
                ([FIRSTNAME]
                ,[FULLNAME]
                ,[CPF]
                ,[BIRTHDATE]
                ,[PHONENUMBER]
                ,[EMAIL]
                ,[PASSWORD]
                ,[ADDRESS]
                ,[NUMBER]
                ,[NEIGHBORHOOD]
                ,[CITY]
                ,[CEP]
                ,[UF]
                ,[STATUS]
                ,[EMAILCONFIRMED]
                ,[ACCESSFAILEDCOUNT]
                ,[CREATIONDATE])
             VALUES
                (@FIRSTNAME
                ,@FULLNAME
                ,@CPF
                ,@BIRTHDATE
                ,@PHONENUMBER
                ,@EMAIL
                ,@PASSWORD
                ,@ADDRESS
                ,@NUMBER
                ,@NEIGHBORHOOD
                ,@CITY
                ,@CEP
                ,@UF
                ,@STATUS, 1, 0, GETDATE())
                SELECT CAST(scope_identity() AS int)");
        }
    }
}