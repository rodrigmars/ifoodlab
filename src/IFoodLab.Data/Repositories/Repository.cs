﻿using Dapper;
using IFoodLab.Domain.Interfaces;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;

namespace IFoodLab.Data.Repositories
{
    public class Repository<T> : IRepository<T> where T : class
    {
        private readonly IDbTransaction _transaction;
        private readonly IDbConnection _dbConnection;

        public Repository(IDbConnection dbConnection, IDbTransaction transaction = null)
        {
            _dbConnection = dbConnection;
            _transaction = transaction;
        }

        #region Operations Async

        public async Task<int> AddAsync(T entity, string query)
            => await _dbConnection.ExecuteScalarAsync<int>(query, entity, _transaction);

        public async Task<T> GetAsync(object obj, string query)
            => await _dbConnection.QueryFirstOrDefaultAsync<T>(query, obj);

        public async Task<IEnumerable<T>> GetAllAsync(string sqlQuery)
            => await _dbConnection.QueryAsync<T>(sqlQuery);

        public async Task<int> UpdateAsync(int id, string sqlQuery)
            => await _dbConnection.ExecuteAsync(sqlQuery, new { id });

        public async Task<int> DeleteAsync(int id, string sqlQuery)
            => await _dbConnection.ExecuteAsync(sqlQuery, new { id });

        #endregion
    }
}
