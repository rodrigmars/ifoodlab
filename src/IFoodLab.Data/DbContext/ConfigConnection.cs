﻿using Microsoft.Extensions.Configuration;

namespace IFoodLab.Data.DbContext
{
    public class ConfigConnection
    {
        private static IConfiguration _config;

        public ConfigConnection(IConfiguration config) => _config = config;

        public static string ConnectionString { get => _config.GetConnectionString("DefaultConnection"); }
    }
}
