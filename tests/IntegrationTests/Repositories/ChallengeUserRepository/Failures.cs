﻿using Xunit;

namespace IntegrationTests.Repositories.UserRepositoryTests
{
    public class Failures : IClassFixture<ServiceBase>
    {
        private ServiceBase Service { get; }

        public Failures(ServiceBase service) => Service = service;
    }
}
