using IFoodLab.Domain.Entities;
using IFoodLab.Domain.Interfaces;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;

namespace IntegrationTests.Repositories.UserRepositoryTests
{
    public class Positive : IClassFixture<ServiceBase>
    {
        private ServiceBase Service { get; }

        public Positive(ServiceBase service) => Service = service;

        [Fact]
        public async Task Checkifauserhasbeencreated()
        {
            using (var uow = Service._servicesProvider.GetService<IUnitOfWork>())
            {
                uow.OpenAsync();

                var query = @"INSERT INTO [dbo].[User]
           ([firstname]
           ,[fullname]
           ,[cpf]
           ,[birthdate]
           ,[PhoneNumber]
           ,[email]
           ,[password]
           ,[address]
           ,[Number]
           ,[Neighborhood]
           ,[City]
           ,[CEP]
           ,[UF]
            ,[STATUS]
            ,[EMAILCONFIRMED]
            ,[ACCESSFAILEDCOUNT]
            ,[CREATIONDATE])
        VALUES
           (@FIRSTNAME
           ,@FULLNAME
           ,@CPF
           ,@BIRTHDATE
           ,@PHONENUMBER
           ,@EMAIL
           ,@PASSWORD
           ,@ADDRESS
           ,@NUMBER
           ,@NEIGHBORHOOD
           ,@CITY
           ,@CEP
           ,@UF
           ,@STATUS
            ,1
            ,0
            ,GETDATE())

        SELECT CAST(scope_identity() AS int)";

                var userId = await uow.UserRepository.AddAsync(new User("Igor"
               , "Igor Nelson Vicente Moura"
               , 83093387833
               , DateTime.Parse("1996-09-08")
               , "(11) 99517-4929"
               , "igornelsonvicentemoura-97@futureteeth.com.br"
               , "Z02OK3KiM9"
               , "Rua Maria de Lourdes Barbosa Carleto"
               , "928"
               , "Concei��o"
               , "Osasco"
               , "06140-100"
               , "SP", true), query);

                Assert.True(userId > 0);
            }
        }

        [Theory]
        [InlineData(1)]
        [InlineData(2)]
        [InlineData(3)]
        public async Task CheckIfUserExistis(int id)
        {
            using (var uow = Service._servicesProvider.GetService<IUnitOfWork>())
            {
                uow.OpenAsync();

                var query = @"SELECT 
                                U.FIRSTNAME, 
                                U.CPF, 
                                U.EMAIL, 
                                U.BIRTHDATE
                                FROM [User] AS U WHERE U.ID = @ID";

                var result = await uow.UserRepository.GetAsync(new { id }, query);

                Assert.NotNull(result);
            }
        }

        [Fact]
        public async Task Checkifthereisuserlist()
        {
            using (var uow = Service._servicesProvider.GetService<IUnitOfWork>())
            {
                uow.OpenAsync();

                var query = @"SELECT 
                                U.FIRSTNAME, 
                                U.CPF, 
                                U.EMAIL, 
                                U.birthdate
                                FROM[User] AS U";

                var results = await uow.UserRepository.GetAllAsync(query) as List<User>;

                Assert.True(results.Count > 0);
            }
        }

        [Theory]
        [InlineData(1)]
        public async Task CheckUpdateUser(int id)
        {
            using (var uow = Service._servicesProvider.GetService<IUnitOfWork>())
            {
                uow.OpenAsync();

                var query = @"SELECT
                                U.ID AS USERID,
                                U.FIRSTNAME, 
                                U.CPF, 
                                U.EMAIL, 
                                U.BIRTHDATE
                                FROM [User] AS U WHERE U.ID = @ID";

                var user = await uow.UserRepository.GetAsync(new { id }, query);

                Assert.NotNull(user);

                query = @"UPDATE [USER] SET STATUS = 0 WHERE ID = @ID";

                var result = await uow.UserRepository.UpdateAsync(user.UserId, query);

                Assert.True(result > 0);
            }
        }

        [Theory]
        [InlineData(2)]
        public async Task CheckIfInactivateUser(int id)
        {
            using (var uow = Service._servicesProvider.GetService<IUnitOfWork>())
            {
                uow.OpenAsync();

                var query = @"SELECT U.ID AS USERID FROM [User] AS U WITH (NOLOCK) WHERE U.ID = @ID";

                var user = await uow.UserRepository.GetAsync(new { id }, query);

                Assert.NotNull(user);

                query = @"UPDATE [User] SET STATUS = 0 WHERE ID = @ID";

                var inactivated = await uow.UserRepository.UpdateAsync(user.UserId, query);

                Assert.True(inactivated > 0);
            }
        }
    }
}