﻿using Dapper;
using FluentValidation;
using IFoodLab.Data.Behavior;
using IFoodLab.Data.DbContext;
using IFoodLab.Data.Repositories;
using IFoodLab.Data.UOW;
using IFoodLab.Domain.Behavior;
using IFoodLab.Domain.Interfaces;
using IFoodLab.Domain.Notifications;
using MediatR;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Data.SqlClient;
using System.IO;
using System.Reflection;

namespace IntegrationTests
{
    public class ServiceBase
    {
        protected readonly IConfigurationRoot _config;

        public IServiceProvider _servicesProvider { get; private set; }

        public ServiceBase()
        {
            _config = new ConfigurationBuilder()
                .SetBasePath(basePath: Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
               .Build();

            _servicesProvider = ConfigureInhector();

            SeedDatabase();
        }

        private IServiceProvider ConfigureInhector()
        {
            var domainAssembly = AppDomain.CurrentDomain.Load("IFoodLab.Domain");

            var services = new ServiceCollection();

            AssemblyScanner.FindValidatorsInAssembly(domainAssembly)
                .ForEach(result => services.AddScoped(result.InterfaceType, result.ValidatorType));

            services.AddSingleton<IConfiguration>(_config)
            .AddTransient<IUnitOfWork, UnitOfWork>()
            .AddScoped(typeof(IRepository<>), typeof(Repository<>))
            .AddScoped<IUserRepository, UserRepository>()
            .AddScoped<NotificationBase, ResponseNotification>()
            .AddScoped(typeof(IPipelineBehavior<,>), typeof(FailFastRequestBehavior<,>))
            .AddScoped(typeof(IPipelineBehavior<,>), typeof(UnitOfWorkAsyncBehavior<,>))
            .AddMediatR(domainAssembly);

            return services.BuildServiceProvider();
        }

        private void SeedDatabase()
        {
            var query = @"
DROP TABLE [dbo].[User]

CREATE TABLE [dbo].[User](
	[id] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[FirstName] [varchar](50) NOT NULL,
	[FullName] [varchar](150) NOT NULL,
	[CPF] [bigint] NOT NULL,
	[BirthDate] [date] NOT NULL,
	[PhoneNumber] [varchar](20) NOT NULL,
	[Email] [varchar](150) NOT NULL,
	[Password] [varchar](10) NOT NULL,
	[Address] [varchar](200) NOT NULL,
	[Number] [varchar](20) NOT NULL,
	[Neighborhood] [varchar](50) NOT NULL,
	[City] [varchar](50) NOT NULL,
	[CEP] [varchar](10) NOT NULL,
	[UF] [char](2) NOT NULL,
	[Status] [bit] NOT NULL,
	[EmailConfirmed] [bit] NOT NULL,
	[AccessFailedCount] [int] NOT NULL,
	[CreationDate] [datetime] NOT NULL,
 CONSTRAINT [user_pk] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [user__un] UNIQUE NONCLUSTERED 
(
	[CPF] ASC,
	[Email] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

INSERT INTO [DBO].[USER]
        ([FIRSTNAME]
        ,[FULLNAME]
        ,[CPF]
        ,[BIRTHDATE]
        ,[PHONENUMBER]
        ,[EMAIL]
        ,[PASSWORD]
        ,[ADDRESS]
        ,[NUMBER]
        ,[NEIGHBORHOOD]
        ,[CITY]
        ,[CEP]
        ,[UF]
        ,[STATUS]
	    ,[EMAILCONFIRMED]
	    ,[ACCESSFAILEDCOUNT]
	    ,[CREATIONDATE])
     VALUES
        ('Fabiana'
        ,'Fabiana Malu Aragão'
        ,27593973867
        ,'1996-07-25'
        ,'(11) 3889-8092'
        ,'fabianamaluaragao@lnaa.com.br'
        ,'Yb2oJckhWW'
        ,'Rua Antônio Borba'
        ,'865'
        ,'Vila Madalena'
        ,'São Paulo'
        ,'05451-070'
        ,'SP' 
        ,1, 1, 0, GETDATE())

INSERT INTO [DBO].[USER]
        ([FIRSTNAME]
        ,[FULLNAME]
        ,[CPF]
        ,[BIRTHDATE]
        ,[PHONENUMBER]
        ,[EMAIL]
        ,[PASSWORD]
        ,[ADDRESS]
        ,[NUMBER]
        ,[NEIGHBORHOOD]
        ,[CITY]
        ,[CEP]
        ,[UF]
        ,[STATUS]
        ,[EMAILCONFIRMED]
        ,[ACCESSFAILEDCOUNT]
        ,[CREATIONDATE])
     VALUES
        ('Thiago'
        ,'Thiago Luiz Rafael Bernardes'
        ,27891892857
        ,'1986-11-19'
        ,'(11) 99579-9526'
        ,'thiagoluizrafaelbernardes@gmx.de'
        ,'elAuW0pdpn'
        ,'Rua Rinópolis'
        ,'889'
        ,'Rudge Ramos'
        ,'São Bernardo do Campo'
        ,'09631-070'
        ,'SP' 
        ,1, 1, 0, GETDATE())

INSERT INTO [DBO].[USER]
        ([FIRSTNAME]
        ,[FULLNAME]
        ,[CPF]
        ,[BIRTHDATE]
        ,[PHONENUMBER]
        ,[EMAIL]
        ,[PASSWORD]
        ,[ADDRESS]
        ,[NUMBER]
        ,[NEIGHBORHOOD]
        ,[CITY]
        ,[CEP]
        ,[UF]
        ,[STATUS]
        ,[EMAILCONFIRMED]
        ,[ACCESSFAILEDCOUNT]
        ,[CREATIONDATE])
     VALUES
        ('Agatha'
        ,'Agatha Hadassa Almada'
        ,25234623890
        ,'1993-08-02'
        ,'(11) 98421-9572'
        ,'agathahadassaalmada-86@dmadvogados.com'
        ,'zL0CepsxLH'
        ,'Rua Jacaretinga'
        ,'953'
        ,'Cidade Recreio da Borda do Campo'
        ,'Santo André'
        ,'09134-030'
        ,'SP' 
        ,1, 1, 0, GETDATE())";

            using (var conn = new SqlConnection(_config.GetConnectionString("DefaultConnection")))
            {
                conn.Open();
                conn.QueryMultiple(query);
            }
        }
    }
}