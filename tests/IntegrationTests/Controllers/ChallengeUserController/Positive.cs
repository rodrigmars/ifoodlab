﻿using Microsoft.Extensions.DependencyInjection;
using MediatR;
using System.Threading.Tasks;
using Xunit;
using IFoodLab.API.Controllers.V1;
using Microsoft.AspNetCore.Mvc;
using IFoodLab.Domain.Commands;
using System;
using FluentAssertions;
using IFoodLab.Domain.DTO;
using System.Collections.Generic;

namespace IntegrationTests.Controllers.ChallengeUserController
{
    public class Positive : IClassFixture<ServiceBase>
    {
        private ServiceBase Service { get; }

        public Positive(ServiceBase service) => Service = service;

        [Fact]
        public async Task CheckOkResultWhenCreateUser()
        {
            using (var scope = Service._servicesProvider.CreateScope())
            {
                UsersController controller = new UsersController(scope.ServiceProvider.GetService<IMediator>());

                var result = await controller.CreateAsync(new CreateUserCommand
                {
                    FirstName = "Stella",
                    FullName = "Stella Alice Patrícia Cardoso",
                    CPF = 58651391854,
                    BirthDate = DateTime.Parse("10/23/1988"),
                    PhoneNumber = "(19) 98283-6945",
                    Email = "stellaalicepatriciacardosostellaalicepatriciacardoso@integrasjc.com.br",
                    Password = "km1Ifdrqxo",
                    Address = "Rua Padre Civeta 7",
                    Number = "232",
                    Neighborhood = "Centro",
                    City = "Monte Mor",
                    CEP = "13190-970",
                    UF = "SP",
                    Status = true
                });

                var okResult = result.Should().BeOfType<CreatedAtActionResult>().Subject;

                var user = okResult.Value.Should().BeAssignableTo<UserCommand>().Subject;

                user.UserId.Should().Be(4);
            }
        }

        [Fact]
        public async Task CheckGetAllUsers()
        {
            using (var scope = Service._servicesProvider.CreateScope())
            {
                UsersController controller = new UsersController(scope.ServiceProvider.GetService<IMediator>());

                var result = await controller.GetAsync();

                var objResult = result.Should().BeOfType<OkObjectResult>().Subject;

                objResult.Value.Should().BeAssignableTo<List<UserDTO>>().Subject
                    .Should().HaveCount(3);
            }
        }

        [Theory]
        [InlineData(1)]
        [InlineData(2)]
        [InlineData(3)]
        public async Task CheckOkResultWhenCallUserById(int id)
        {
            using (var scope = Service._servicesProvider.CreateScope())
            {
                var controller = new UsersController(scope.ServiceProvider.GetService<IMediator>());

                var actualResult = await controller.GetAsync(id) as OkObjectResult;

                actualResult.Should().BeOfType<OkObjectResult>()
                    .Subject.Value.Should().BeOfType<UserDTO>().And.NotBeNull();
            }
        }
    }
}