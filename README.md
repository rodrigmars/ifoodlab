# IFoodLab

#### API server para laboratório de transações on-line.

> BTW: *Estórias de usuário são a base de escrita para **_casos de uso_***


Requisitos
----------

 - Gateway API para requisições assíncronas

 - Arquitetura limpa orientada a negócio para operações de leitura e escrita 
 
 - Middleware para Logs e Notificações(SMTP e Push)

Tecnologias
----------

 - ASP.NET Core 2.1 WebApi e demais projetos

 - Micro ORM Dapper não optante por Fluent 

 - Suporte a SQL Server

 - TDD em xUnit